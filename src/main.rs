use std::time::Instant;

use num::complex::Complex;
use num::traits::Float;
use num::FromPrimitive;
use sdl2::event::{Event, EventType};
use sdl2::mouse::MouseState;
use sdl2::pixels::Color;
use sdl2::rect;

use rayon::prelude::*;

fn iters_at_point<T: Float + Send>(p: (T, T), max_iters: usize, escape_value: T) -> usize {
    let mut z = Complex::<T>::new(T::zero(), T::zero());
    let c = Complex { re: p.0, im: p.1 };
    for i in 0..max_iters {
        if z.norm() > escape_value {
            return i;
        }
        let z_conj = z.conj();
        z = z_conj * z_conj + c
    }
    return max_iters;
}

fn tricone_range
//<T: Float + FromPrimitive + Send>
(
    min_x: f64,
    max_x: f64,
    x_points: usize,
    min_y: f64,
    max_y: f64,
    y_points: usize,
    max_iters: usize,
    escape_value: f64,
) -> Vec<Vec<usize>> {
    let delta_x = (max_x - min_x) / f64::from_usize(x_points).unwrap();
    let delta_y = (max_y - min_y) / f64::from_usize(y_points).unwrap();
    (0..y_points).into_par_iter().map(|row| {
        (0..x_points).into_iter().map(move |col| {
            let y = max_y - f64::from_usize(row).unwrap() * delta_y;
            let x = min_x + f64::from_usize(col).unwrap() * delta_x;
            iters_at_point((x, y), max_iters, escape_value)
        }).collect()
    }).collect()
}

fn iter_to_color(iters: usize) -> Color {
    Color::RGB(
        ((iters & 0xF) << 4) as u8,
        (((iters >> 4) & 0xF) << 4) as u8,
        (((iters >> 8) & 0xF) << 4) as u8,
    )
}

fn main() -> Result<(), String> {
    let img_size: i32 = 800;
    let zoom_factor = 0.1;
    let sdl = sdl2::init()?;
    let mut canvas = sdl
        .video()?
        .window("tricone fractal viewer", img_size as u32, img_size as u32)
        .opengl()
        .build()
        .map_err(|e| e.to_string())?
        .into_canvas()
        .build()
        .map_err(|e| e.to_string())?;

    let mut min_x = -2.;
    let mut max_x = 2.;
    let mut min_y = -2.;
    let mut max_y = 2.;
    let evel_img = move |min_x, max_x, min_y, max_y| {
        let beg = Instant::now();
        let img = tricone_range(
            min_x,
            max_x,
            img_size as usize,
            min_y,
            max_y,
            img_size as usize,
            2048,
            100.,
        );
        let end = Instant::now();
        let time_taken = end - beg;
        println!("time taken: {}", time_taken.as_millis());
        img
    };
    let mut render_img = move |min_x, max_x, min_y, max_y| {
        let img = evel_img(min_x, max_x, min_y, max_y);
        for (x, row) in img.iter().enumerate() {
            for (y, pix) in row.iter().enumerate() {
                canvas.set_draw_color(iter_to_color(*pix));
                // canvas.fill_rect(rect::Rect::new(x as i32, y as i32, 2, 2))?;
                canvas
                    .draw_point(rect::Point::new(x as i32, y as i32))
                    .unwrap();
            }
        }
        canvas.present();
    };

    render_img(min_x, max_x, max_y, min_y);
    let mut events = sdl.event_pump()?;
    events.enable_event(EventType::MouseWheel);
    'infLoop: loop {
        let event = events.wait_event();
        match event {
            Event::Quit { .. } => break 'infLoop,
            Event::MouseWheel { .. } => {
                let stat = MouseState::new(&events);
                let x = stat.x();
                let y = stat.y();
                let rel_x = x as f64 / img_size as f64;
                let delta_min_x = zoom_factor * rel_x;
                let delta_max_x = zoom_factor * (1. - rel_x);
                let rel_y = y as f64 / img_size as f64;
                let delta_min_y = zoom_factor * rel_y;
                let delta_max_y = zoom_factor * (1. - rel_y);
                min_x += delta_min_x;
                min_y += delta_min_y;
                max_x -= delta_max_x;
                max_y -= delta_max_y;
                println!("zoom into {},{}", x, y);
                println!("min x: {}, max x: {}", min_x, max_x);
                println!("min y: {}, max y: {}", min_y, max_y);
                render_img(min_x, max_x, max_y, min_y);
            }
            _ => {}
        }
    }
    Ok(())
}
